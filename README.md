# commons.garden

- [commons.garden](https://commons.garden)
- [our mission](https://commons.garden/about/mission)
- [our TODO](https://commons.garden/about/todo)
- [our milestones](https://commons.garden/about/phases)


## INSTALLATION

1. install Hugo: https://gohugo.io/installation/
2. Ask for editing access
3. Clone down this repo 
  - Copy this URL [https://gitlab.com/commons-garden/commons-garden.gitlab.io.git](https://gitlab.com/commons-garden/commons-garden.gitlab.io.git)
 - In terminal: "git clone COPIEDPATH (PATH to the directory you want the files in)"

## EDITING

1. On terminal, go to repository folder 
2. Make sure to update changes with `git pull`
3. Run Hugo locally with `hugo server -D`
4. Open [your local version](http://localhost:1313/) on the browser
5. Open your code editing app, edit away

## PUBLISHING

1. Make sure to update changes with `git pull`
2. Add your changes with `git add -A`
3. Commit your changes: `git commit -m "comment message here"`
4. Push your changes: `git push`

Gitlab will run the flag blog again once you push.
