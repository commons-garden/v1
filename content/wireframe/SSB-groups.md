---
title: SSB groups
type: public
description: Private Groups – User Experience
status: stable
scope: SSB
author: [nonlinear]
reviewed: [staltz, mixmix, jacob, arj]
project: ssb-groups
soon: [ahau, manyverse]
category: wireframe
date: 2023-05-19
link: https://www.manyver.se/blog/2023-05-19
---

Groups are important because SSB is a cloud-less peer-to-peer platform, where discourse is generally open (like Twitter).

Adding groups means communities can coordinate, share memories, manage resources, away from prying eyes, protecting their sovereignty.

Currently the two apps spearheading the new feature are Manyverse and [Āhau](https://ahau.io/) (a data-sovereignty-minded app built by and for indigenous groups) but others will come.
