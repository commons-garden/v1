---
title: Project phases
display: public
status: ongoing
description: Our milestones
category: about
author: [nonlinear]
date: 2023-09-10
---
## Phase 1 (complete)

- specs category
- about category
- initiatives category
- wireframes category

## Phase 2 (now)

- members category
- organizations category
- wire categories across
- manual category (template,s criteria, etc)
- discuss glossary, living documentation (airtable?)
- donation platform (open collective?)
- discuss announcements (newsletter? telegram?)

## Phase 3 (next)

- find facilitators
- facilitate for facilitators
- volunteer rotation

