---
title: Goals
display: draft
description: What we want and how we go about it
category: about
author: [nonlinear]
date: 2023-11-01
---

- Foster a community of effective and gentle volunteering
- Making commons' projects legible to everyone, so collaboration can form with less friction
	- Empower facilitators to document initiatives, meetings and deliverables so we have honest, up-to-date statuses on projects
		- Reduce learning curve for proper documentation:
 markdown, git + shared notes, writing standards (naming convention, frontmatter criteria, tags)
- Sponsor initiatives to discuss seminal/existential topics
- A formalized way to request (needs) and offer help (volunteering)

## Tools needed:

1. A way to expose subpages properly
1. git + shared notes (hackmd?)
1. "needs" tag (glossary)
1. "slide" mode (landscape)
1. how to surface git history on pages
1. A path to train facilitators