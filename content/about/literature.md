---
title: Literature
display: public
description: Our Readings
status: ongoing
category: about
date: 2023-09-06
---


## Literature to Understand and Inspire

- Starter guidelines - [Digital Garden Terms of Service](https://www.swyx.io/digital-garden-tos)
- Tools and Resources - [Digital Gardeners](https://github.com/MaggieAppleton/digital-gardeners)
- Use What You Make - [What is the IndieWeb?](https://indieweb.org)
- [A Brief History & Ethos of the Digital Garden](https://maggieappleton.com/garden-history)
- [Anarchist Cybernetics](https://anarchiststudies.org/acybernetics/)

## Digital Gardens We Like

- [Tom Critchlow](https://tomcritchlow.com/wiki/)
