---
title: Knowledge Ecology
display: public
status: ongoing
description: Field coherence discussion
category: initiative
author: [mixmix]
facilitator: [nonlinear]
date: 2023-10-03
---

## Welcome! 

*As we exchange, define, refine, and circle back to ideas together, some of us may find knowledge synthesis useful. This document is a starting point for that synthesis. **Distributed processing is much appreciated** - decentralise labour and power!*

## :crystal_ball: Origin Story
*The group chat space this document emerged from originated in a small spell.* :mage: 

Once upon a time, a knowledge ecologist met a spell-casting, community-fostering coder...

<details>
  <summary><strong>Discover the story</strong></summary>
Once upon a time, a knowledge ecologist met a spell-casting, community-fostering coder. As they spoke, wrote, and thought together it quickly became evident to both of them that, among many other things, knowledge ecosystems were a shared interest space. 

The code wizard, having not heard the term **[knowledge ecology]()** before (*ADD LINK, when ready*), wanted to learn more. But the ecologist, busy with her research, daunted by the prospect and scope of conveying what she was still learning herself, was afraid to start teaching, or even talking much about what she'd been percolating on alongside her work. She had thought those ideas about her perspectives and methods of research safe on a back-burner for now, to be stirred later. 

So she asked for patience. 

Smiling quietly, the coder cast a small spell. :game_die: Turning that spot on her waitlist into a group chat space between them, added a friend for the magic of three, and - only then - waited. 

Although it was a risk, the spell worked. :sparkles: 

The ice of fear around the ecologist's heart melted, and she started to write. To herself, mostly, and adding occasional words out loud in the small space, along with a few of her most trusted collaborators. 

The space grew. :books: :bookmark_tabs: :open_book: :hole::rabbit2: 

The code magician helped hold and grow the new space, adding a few more people with perspectives generative to what was emerging. And so it goes.
  
#### :round_pushpin: This is where you are now. 
Welcome to the magic.
:boom: 
</details>

## :hand:Boundary request
> *"The first thing to do is to eliminate things. It’s much harder to think about complexity when you’re thinking about things interacting with other things."* - Lauren H.
  
We'll need others, and the tiny group chat we started with won't contain multitudes, so before adding new people, please consider: 
<details>
  <summary><strong>Open to consider the boundaries</strong></summary>
    
- Are you diving in an adjacent space? We're happy to work out coordination and learning over time with overlapping spaces. 
- We hope that folks here:  
    - have a deep desire to learn and even shape this baby field of knowledge ecology as it emerges,  
    - understand that we'll need both strong grounding in ecological science, and other "web of life" ways of knowing, and of being. :revolving_hearts: 
    - lots of relevant rabbit holes in adjacent fields exist. Lots, including computer science, social science, and philosophy, have perspecitives on knowledge. We are here in this first group chat under this small first spell not to explore all the rabbit holes, but to feel out the fuzzy boundaries of knowledge ecology as a study, a practice, and a field of inquiry. 
</details>

---

## Synthesis 

_Below are some sections you can contribute to, but feel free to start new ones as well_


### :mag: Great Lenses / Questions

> *What questions did you hear asked which helped surface boundaries?*

- Could we play a game like "name the knowledge ecology" where someone describes a scenarion and we try to highlight the aspects/influences/recommendations that are "knowledge ecology"? 
- How can we foster rich and diverse knowledge ecologies? 
- What are the low level aspects of knowledge we're discussing? When you say "ecology" it changes the lens from a static taxonomy to a moving weaving dance and interplay of knowings. 
- How do knowledge and belief differ?
- What role does mutualism play in a vibrant knowledge ecosytem?
    - could look at mutually beneficial relationships between orgs? Opportunities for symbiotic relationships. Interesting. 
- what are the patterns we need antidotes for? (like silos)
- Is health in some part about what I'm calling "wisdom"?
- Does "knowledge" at an ecosystem level look like recurring patterns of behaviour?
- How does an ecological perspective help me establish more monetary+resouce sharing in my community?
- How do higher order patterns and flows emerge? 
- Where is the knowledge? *(ie, in a city system)*
- What is harm? Who defines it?
- low-trust vs high-trust societies... how to detect and move from one to another? 
## :seedling: Future Sprouts

> *We saw some fresh seedings starting to peek out in this conversation. If you could start or continue to grow one, what would it be?*
### Within this context...
- a recurring time to talk
- **Other spaces** 
    - Could we dogfood [Socialroots](https://www.socialroots.io/)? (Lauren / Christina)
        - will add more about this later
    - A forum? (nonlinear)
    - A shared LogSeq? (christina)
    - A space for knowledge ecology knowledge garden on https://commons.garden/ (mix, christina) 
    - ? 
- a 'living knowledge' pattern exploration across contexts
- identifying key patterns and concepts that need great explainers, or better, explorables ala https://ncase.me

### In wider contexts...
- a data structure called "tangles"...
    - *CRDTs operations collected in a DAG (directed acyclic graph), along with rules about who is allowed to extend the DAG, and how. But yeah, the result is records being litterally the sumation of a tapestry of contributions, and that the reading of it can be subjective. It feels like a reflection of reality.* - mix

- ...
- ...
## :thinking_face: Reflections

- not here, but in another space, would love to further explore best practices for #LocalFirst software
- ... 

## :books: Books
> We have a [knowledge ecology booklist](https://bookshop.org/lists/knowledge-structures-and-flows) on bookshop dot org. Let Christina know if you'd like something added there, or add books here: 
- ...


## :accept: Definitions

> Several words will emerge that we need to ground with a common understanding of what we mean when we gesture that way. Here's an emergent glossary of terms. :closed_book: 

### Knowledge Ecology
    initial takes
- (mix) systems of knowledge - human knowledge as cultural rituals/ hard won models of the world, and how these perspectives form complexes. Kinda dawkins' "meme complexes" etc.
- (pospi) fuzzy still, but: mapping the relationships between humans and groups of humans and how that informs the flow of knowledge 
    - marrying ontology between different formal and informal systems of meaning and deciding when to marry and when to sever meaning
    - how do you rigorously understand epistemology behind an ontology / sources of knowledge and where the knowledge comes from
    - how do you weave in a responsible manner that is respectful of difference and pluralistic in nature and balance that with mismatched ideologies 
    - how do you translate knowledge across local instances of context and meaning 
- ...you?

#### Knowledge
- Knowledge, like mind, like self, is a verb, a cocreated process that happens as we interact in knowledge ecosystems. 
- I experience "knowledge" as a uniquely human co-creation between humans. (Not to suggest that other entities do not possess consciousness and knowledge of their own, but from a human vantage point that knowledge is mostly unintelligible and so mostly cannot fall under the scope of us humans discussing what we mean by "knowledge".)
- graphs + fields = yes!
    > *The graph is one of those extremely flexible model ideas so it's not surprising you can fit some ideas about ecologies into them. But even tight systems like brains have more stuff going on than a graph accounts for. Each nueron is bathed in a local stew of chemistry, modulated by the rest of the body, as it connects to other neurons, affecting the nature of the signal (a simplification of electrochemical discussion between two or three or ten nuerons), and the rest of the body absorbs signaling from what it has eaten, what it's sensorium is producing, how bacteria are interacting with it. Really, the fact that anything coherent comes out of all complexity that seems like chaos is amazing.*
 ***So maybe a combination of graphs and fields?*** - Khaled
 - Knowledge is a flow - latent knowledge is how we usu think of it, but you're either able to draw on it / act on it in a given moment or not. this is why i think access (bounded) is impt for democratizing strategic knowledge / ability to adapt to change, and so improve heath. **It's sort of like electricity. if it's not in flow, you can't use it.** 
 
 [digression on "ba"] (add link)
 
 
![](https://hackmd.io/_uploads/Hym4vqukT.jpg)
- "Knowledge is one of the primary mental filters for how individuals understand and make sense of the world." - Phillip Ellis
- ...


#### Ecosystem
An ecosystem is a fuzzy-boundaried space with interacting agents (usually organisms) cocreating themselves in relationship to each other and the environment/ context.
Ecosystems have networks, flows, cycles.

- ecology = study of home
- ecosystem = interacting system of agents in flows and contexts (or somesuch) 

If we're talking about the methods / study of / perspective on => ecology
if we're talking about a landscape / context / etc => ecosystem 
if we're talking about just the interacting agents, it's usu community or population, but those words are a bit fraught.

- boundaries in complex systems are arbitrarily defined. Really all complex adaptive systems are processes of transcontextual mutual learning
- ...
- ...

#### Health

#map [Map of health as dynamic balance](https://kumu.io/GENIE/integrative-health#a-systems-view-of-health) christina made years ago from the Systems View of Health chapter of The Systems View of Life by Fritjof Capra and Pier Luigi Luisi
![](https://hackmd.io/_uploads/rySorADJT.png)

- the importance of pluralism as an ingredient in "health"
    - "the presence of counterculture" (ref Bartlett)
    - a bunch of stuff in there too about tolerance of disagreements and forking and how tightly coordinated different group actors need to be
- gratitude. Measure expressions of gratitude.
- on health in orgs and org networks
    - the idea of orgs as entities in their own right is a very Western concept which leads to a lot of damage being done... The org becomes an idea to anchor bad behavior to, erasing individual responsibility. 
    - strength of interpersonal relationships *between* orgs
    - Siloing certainly seems unhealthy
    - [to see healthy] human dynamics...look at signs of poor health and think of opposites. But we don't even have to! https://www.whitesupremacyculture.info/characteristics.html lists "antidotes"
- A healthy ecology is stable and adaptive. We're further interested generative ecosystems (create novel patterns which explore possibility space of persisting patterns)
- Diversity (or, redundancy) makes for a healthy ecology. Lack of diversity/redundancy makes it vulnerable to unknown threats. 
- ...

### Learning
@lauren, want to add here?

*boundaries in complex systems are arbitrarily defined. Really all complex adaptive systems are processes of transcontextual mutual learning...* - lauren

### Worldview
on [Worldview Cultivation](https://www.notion.so/veeo/Worldview-Cultivation-c0d5a32a67704d44b2ed6e62e0471044?pvs=4), by Lauren H
- an individual's epistemology is not a set of beliefs to be developed, but a network of cognitive capacities that must be accessed and activated, depending on the situation.- Phillip Ellis
- 

### Ecological fitness
... ? 
> There are three sorts of ecological fitness: the well-documented ability to *compete*, the ability to *cooperate* (as in mutualistic symbiosis), and a third sense of fitness that has received insufficient attention in evolutionary theory, the ability to *construct*.”

from https://www.sciencedirect.com/science/article/abs/pii/S1369848610001044




## :pushpin: Pins

> You can use "pins" to mark comments as significant in some way. If you like, copy those comments in here, and mark them DONE in the chat with a "tick" :heavy_check_mark: 

- "Data between people should be held between people." - mix
- "All personal data is actually interpersonal data" - sheldrake
- Epistemic thought has a spectrum, and as we get better at critical thinking we recognize more the blurred line between subjective and objective. (ref Deleuze?)
- that's where we get into dangerous territory cos then everyone wants to quantify "health" and people do things like apply Integral Theory to community economic models http://metaintegral.com/
-  Knowledge ecologies ≠ knowledge monocultures (maybe ecosystems?)
-  "Wisdom smells like an emergent property of a healthy knowledge ecosystem" (christina)
-  "effectiveness often comes from a healthy dynamic tension between efficiency and resilience" (lauren)
- in the brain knowledge is not held in neurons... it's the graph of neurons. (mix)
- #GraphsInContextAreMAPS (christina)
- some knowledge stays in between two nodes (nonlinear)
    - (some properties emerge only at the level of the system)
- (mix) the knowledge exchange that is happening around Holochain from my perspective is:
    - agents messaging about potential pollution / traps
    - concern is about the cycles/ patterns which are collapsing diversity
    - less diversity = dangerous for some agents, and less resilience and generativity for that part of the ecosystem
- I think knowledge ecologies have topologies to them, and those topologies determine *how* information is able to flow. In human social ecologies, the peaks and troughs (sometimes chasms) in the topology are variously created and inhibited by empathy, ideology, self-reflectiveness. This affects knowledge transmission in the broader containing knowledge ecology within which the social ecosystem is situated. (pospi)

> It doesn't have a to be an ultimatum of "denounce or die", it's a subtle long conversation that teases out the many fibers that compose a person's actions in the world. Maybe you can even influence them over the course of the conversation, but the main thing that influences people is their social network. So the flow of information around and through them as prioritized by who they call their kin really determines the structure their actions will take in the long term. "Denounce or die" causes people to distance themselves, which in the case of needing a safe space is fine, but it prevents the flow of information that could have a positive effect in changing people for the better. This is basically the kind of thing social media has done is to interrupt or monetize this flow of information and mediate how we interact with our kin. - Jillian Burrows
- ...
- ...

## :link: :hole::rabbit2: Rabbit Holes

> Find a link valuable? Add it here for leisurely exploration. Most folks can't jump in to listen to a podcast, watch an insighful video, or read scholarly paper, as they catch up in a group chat. Let's make it easy to circle back!

- #podcast [Scaling Selfhood: Collective Intelligence from Cells to Economies with Michael Levin](https://www.musingmind.org/podcasts/collective-intelligence-cells-economies-cosmos-michael-levin)  (approx 2hrs)
    - bonus! additional [summary and reflections here](https://mcdn.podbean.com/mf/download/yqri7s/Michael_Levin_Patreon_Only_Finalized783pw.mp3)(30 min)
- #video (from pospi) Neighbourhoods and the [continually evolving participatory process](https://youtu.be/ytt1fXd354k?feature=shared) that we are moving into 
    - See also #blog https://economikit.org/blog/2021/05/the-context-why/
- :green_book: #book [Linked: How Everything Is Connected to Everything Else and What It Means for Business, Science, and Everyday Life](https://www.goodreads.com/book/show/34725938-linked) 
- #podcast, on "governance by vibe": [Web 3.0 Xmas Special by The Other Others](
https://podcasters.spotify.com/pod/show/tyson-yunkaporta/episodes/Web-3-0-Xmas-Special-e1c3vnl) with Jordan Hall and Tyson Yunkaportais
- #map #blog an example of why open access data can damage/ extract from communities: [Farm data as value added](https://kumu.io/csageland/food-dynamics)
- #blog on Slime molds and more [Beyond Smart Rocks](https://www.growbyginkgo.com/2020/07/15/beyond-smart-rocks/)
- Play a #game! [Cities: Skylines](https://store.steampowered.com/app/255710/Cities_Skylines/)
    - This is a game where you build roads, tunnels, zone cities (policy), tax, and build things like schools, fire stations, police stations etc. You get deep into flows and optimizing flows of traffic, where people are trying to go to work from residential to industrial or business areas, waste is being collected, fire trucks /ambulance/police are trying to get to situations to stabilise them. It's a mess. (mix)
- #video **[The Speed of Outrage: Tom Scott at Thinking Digital 2015](https://youtu.be/jE2PP7EowdM?feature=shared)**
[![The Speed of Outrage: Tom Scott at Thinking Digital 2015](https://img.youtube.com/vi/jE2PP7EowdM/0.jpg)](https://youtu.be/jE2PP7EowdM?feature=shared)
- ...







