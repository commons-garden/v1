---
title: Pinned comments
display: public
status: ongoing
category: salon
author: [mixmix]
description: best-of signal group
facilitator: [nonlinear]
date: 2023-10-03
---

> From [Knowledge Ecology](/salon/knowledge-ecology/) salon

---

the transferable knowledge about knowledge systems I want:
    - toolkit of surfacing questions
    - health diagnostic suggestions
    - how to know where you are in the evolution/ growth of a knowledge ecosystem
    - how to heal a sick system
    - how to sprout new ecosystems



---


as relates to cells, selves and identities, boundaries, care, and collective intelligences, this summary discussion would be a good ground to start with, though in terms of a standard curriculum, it’s diving in the deep end.
https://mcdn.podbean.com/mf/download/yqri7s/Michael_Levin_Patreon_Only_Finalized783pw.mp3


---


https://www.goodreads.com/book/show/34725938-linked


---


The Church is certainly one of the big institutional knowledge ecologies
Yeah they have a big knowledge ecosystem… but I’ll wager they don’t steward it with the health of the system as the goal

That’s what I thought was neat about P's suggestion that we identify patterns we want antidotes to: find the shape of unhealth

---

But before we get into how expanding the window of tolerance might happen in orgs/ communities/ cultures I think we need to get back to the idea of ecosystem health, then look at how that’s possible to sense in knowledge ecosystems

you know, I still take J's metric as the most generally useful: gratitude. Measure expressions of gratitude.

---

Both stability and change are at the heart of living systems

---

- focus on relationships more than counting or accruing 
- develop and support the agency of others in the process 
- clearly define dynamic boundaries (who’s in, and who/ what can come in, and when)
- orient together around a clear goal and ways to see both baseline and change over the next ___ time, and then (as discussed above) be wary about introducing new goals as driving forces (look out for misaligned incentives)

Knowledge ecology is a practical science. A community is a living system already. The knowledge/ money/ resources you can pull together need generative flows that help foster the things we talked about (learning, experiments, knowledge discovery and creation)

And expect messiness. Because humans.

Also, money is a verb or needs to be, but all the systems surrounding your community treat it as a noun. This, and the “success to the successful” dynamics are the hardest to overcome for the resource-sharing use case in my experience

---

For happy =healthy, that holds at the organism and community scale
But there’s not a system-level “agent” to be happy. Just the sense of flourishing or stressed
The first 3 things to grok in any ecosystem are the network(s), flows, and cycles

---

Seems like the right moment to drop this in : https://www.notion.so/veeo/Worldview-Cultivation-c0d5a32a67704d44b2ed6e62e0471044?pvs=4

---

If knowledge is “information that persists,” and intelligence is “applied knowledge,” then I think all kinds of systems have intelligence without explicit consciousness. A forest, a meadow, an ant colony.

---

It seems like the reaction to stress (clamping up or opening up) is knowledge. The system is learning that it’s safe to be vulnerable during a stressful period or that it isn’t.

---

oooooooh. Now for me *that* is getting close to some core "health" concerns. Monocultures of thought as maladapted knowledge ecologies? Self-reinforcing rather than self-adapting information loops?

---

is this the only thing that would have worked? did it make the city 'healthier' (more resilient to stressors)? 
one thing i like is an approach to work teams where we all do some of the 'take out the trash' work. what would that look like on city scale? 
1/3 time on your core competencies / 1/3 time on learning edge / 1/3 time doing what needs to be done. or something. 

'knowledge', if we take the graph + field approach, might be in the empathy that people would have for the undereducated - that new context might over time would make new changes. does the game model empathy? 

But I think there's still a noun around this 'knowledge' of which you speak - you want to find it somewhere. you're looking for 'where' it lives. but.. (i think)
Knowledge is a flow - latent knowledge is how we usu think of it, but you're either able to draw on it / act on it in a given moment or not. this is why i think access (bounded) is impt for democratizing strategic knowledge / ability to adapt to change, and so improve heath. 

dance is embodied knowledge 

It's sort of like electricity. if it's not in flow, you can't use it. 

The data lakes of noun-like knowledge are like books no one reads. no flow, no knowledge, only latent potential.

---

getting a bit abstract - the knowledge exchange that is happening around Holochain from my perspective is:
- agents messaging about potential pollution / traps
- concern is about the cycles/ patterns which are collapsing diversity
- less diversity = dangerous for some agents, and less resilience and generativity for that part of the ecosystem

---

kalquaddoomi
I'm thinking of the topology when I think about fluidity mostly. Like the topology of life is in environments that we classify into what resources and energy changes occur within their scope, although even that classification breaks down depending on how we apply scale.

ooooh such a rich vein there. I think knowledge ecologies have topologies to them, and those topologies determine *how* information is able to flow. In human social ecologies, the peaks and troughs (sometimes chasms) in the topology are variously created and inhibited by empathy, ideology, self-reflectiveness. This affects knowledge transmission in the broader containing knowledge ecology within which the social ecosystem is situated.
This is starting to make social organisms feel more like slime moulds... which seems like part of our goals here?

---

EXCEPT technology does not live in isolation. Holochain is sociotechnical. The flows matter, the people influence the software. If you community is toxic to diversity, then the only people building the tech, having input etc, will be a monoculture. The technology gets more brittle

---

Listening to a relationships audio book which treats relationships as interlinked neorobiologies (ideally) co-regulating

Anyway in one bit the author says that the corpus collosum which bridges the hemispheres also insulates them from one another - it might behave as a rate limiter?

---

Oh I like this approach. We map thresholds because once we reach them, new properties arrive. Scalability not only is a futile goal, but it's also not smooth.

---

I think there may be multiple strategies for resilience, diversity describing a kind of meta resilience, or maybe a separate strategy all it's own.

---

It's interesting, because certain systems really encourage pathways of high optimization, but have lots of them, like autoimmune systems. Without some really optimized cells, complex bodies would succumb too quickly to very mild infections.


---

https://davidsloanwilson.world/papers-and-studies/two-meanings-of-complex-adaptive-systems/

---

Was at a friends birthday discussing mindfulness (internal system stability) and community stability.
Was also sharing about the signal groups I contribute to to strengthen resiliance. A friend shared about his sangha. His description reminded me of the "karass" from Cat's Cradle (vonnegut)

---

My hypothesis so far: knowledge communities grow if there are available resources for discussion, to discuss is the verbal equivalent of to reproduce in sexual biology. Increasing Communicating leads to increased population, until the carrying capacity of the knowledge platform is reached, at which point either diversity happens and new groups splinter away and occupy new knowledge spaces that fit their population size or the population becomes stressed and starts to overextend and loses coherence.


---


Hmm, reflection is crucial I think. Replication without examination leads to all kinds of problems, which might actually be the biggest problem so called "knowledge" machines (computers) introduce. Biological reproduction has a ton of reflective processes, at the cellular all the way up to mating dances and community reflection.


---

:percolates more on fitness:

“There are three sorts of ecological fitness: the well-documented ability to *compete*, the ability to *cooperate* (as in mutualistic symbiosis), and a third sense of fitness that has received insufficient attention in evolutionary theory, the ability to *construct*.”

from https://www.sciencedirect.com/science/article/abs/pii/S1369848610001044

---

Put another way, environmental boundaries defines (permiable) bounds of a possibility space (eg finite resouces, energy). Then patterns which are generative will evolve and grow to fill that space, discovering stable states (which may depend on neighbouring patterns), and further stable states with respect to short anl long time scales.

---

https://qz.com/1181019/the-japanese-words-for-space-could-change-your-view-of-the-world

---

Yeah because the ontology is different the puzzle pieces of the ecosystem are quite different shaped

---

I dunno. I think my problem with fractals (and spore molds, etc) is that while they are beautiful, they are an oversimplification, and effort, so like science, to take a bottom up approach and seek universal patterns and truths, when nature and life simply do not offer that up, diversity and sometimes complexity, being a beloved component of our universe, and the core stratagem being nothing too alike, all things changing, all things that ever were, are, will be, altering in unplottable ways.

---


Yaaas. The terrain is totally different

I notice us traversing another relationship (perhaps even a causal one) between "ontology" and "terrain "here

---

Quick overview of the people here and relationships @yojo :
- @Christina Bowen + @kalquaddoomi are working on a coop called https://www.socialroots.io/ which is foccusing on inter-group communication. Knowledge and growth *between*
- @nonlinear and I work on https://commons.garden - mapping and facillitating commons initiatives. (I work every 1-2 weeks with this human ❤️ ) 
- @Sid Sthalekar @pospi are connect by https://neighbourhoods.network/ which is building tools to let communities build their own software (composable). They're in NZ + Australia respectively
- @Sheldrake is a friend of Christine's and wrote that article I loved and shared the other day https://philipsheldrake.com/

The others @Micah @Lauren Hebert would you mind introducing yourselves, I haven't talked with you as much

---

https://www.frontiersin.org/articles/10.3389/frma.2021.752336/full

---

But that "now" practice was built from long learning. In maori the word is tikanga - I think it means "protocol" or "right way"

---

*knowlege* ecology. Like why are we focusing on *knowledge*.
I care about pattern health at all the levels

---

Knowledge commons as problem solving resources. They are Taonga, as is the knowledge embodied in and transmitted between them…

---


Yes, exactly. So maybe (until we can understand knowledge as a flow, a verb) you can think of this as problem ecology. 

The problem question is the boundary of a strategic map. In order to agree on what the problem is, I’ve been helping communities and networks develop problem maps / adaptive strategy maps to understand how to begin to address wicked problems. In order to navigate a problem landscape, you need to tap into your knowledge ecosystem


---
-
Above not worth dwelling upon IMO. I came to share this 

"dominator ecology... is the ecology of management from a distance, and of remote expertise, that sees itself as fundamentally separate from the land, inhabiting a present without a past or future.” "five starting points for an anti-colonial, anti-authoritarian way of connecting with the land... are: rooted in relationships, deep listening, urban ecology, re-enchanting, and unexpertness."
https://transportation.social/@colby/111166325651951554

---


I believe the origin of the wicked is this from 1973 defining ten aspects
1. There is no definitiveformulationof a wicked problem
2. Wicked problemshave no stopping rule
3. Solutions to wicked problem sare not true-or-false, but good-or-bad
4. There is no immediate and no ultimate test of a solution to a wicked problem
5. Every solution to a wicked problem is a "one-shot operation"; because there is no
opportunityto learn by trial-and-error, every attempt counts significantly
6. Wicked problemsdo not have an enumerable(or an exhaustively describable)set of
potential solutions, nor is there a well-described set of permissible operations that may be incorporated into the plan
7. Every wicked problem is essentially unique
8. Every wicked problem can be considered to be a symptom of another problem
9. The existence of a discrepancyrepresentinga wicked problem can be explained in
numerous ways. The choice of explanation determines the nature of the problem's
resolution
10. The planner has no right to be wronghttps://www.sympoetic.net/Managing_Complexity/complexity_files/1973%20Rittel%20and%20Webber%20Wicked%20Problems.pdf


---
