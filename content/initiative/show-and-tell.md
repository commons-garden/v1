---
title: show-and-tell
description: gather to learn, ask questions, get pumped, and cheer on your peers
facilitator: [mixmix]
display: public
category: initiative
---

## Tilde Friends

{{<figure 
  src="/images/tilde-friends-screenshot.png"
  width="500px"
>}}

Cory gives a tour of their work on [Tilde Friends](https://www.tildefriends.net/) - a p2p app platform
backed by Secure Scuttlebutt


<!--
{{< rawhtml >}}
<!-- Src: https://www.worldtimebuddy.com/event-widgets -->
<span class="wtb-ew-v1" style="width: 410px; display:inline-block"><script src="https://www.worldtimebuddy.com/event_widget.js?h=2179537&md=9/14/2023&mt=9.00&ml=1.00&sts=0&sln=0&wt=ew-lt"></script><i><a target="_blank" href="https://www.worldtimebuddy.com/">Time converter</a> at worldtimebuddy.com</i><noscript><a href="https://www.worldtimebuddy.com/">Time converter</a> at worldtimebuddy.com</noscript><script>window[wtb_event_widgets.pop()].init()</script></span>
{{< /rawhtml >}}
-->


{{< rawhtml >}}
<iframe width="500" height="315" src="https://www.youtube-nocookie.com/embed/XD74RYm-DIM?si=jdLSUzEpeeb4Mxwg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
{{< /rawhtml >}}


**Corey's Project**

Corey, a game developer from New York, introduces 'Tilde Friends', his new project. It's a platform for creating and sharing apps using something called Secure Scuttlebutt.

**The Tech Behind It**

'Tilde Friends' is built using a programming language called C and runs on both Raspberry Pi and Android devices. It features its own web server, a system for users to create accounts, and a safe area for apps to run.

**Key Features**

Corey demonstrates how to use 'Tilde Friends' for making an account, posting content, and modifying apps. The platform allows users to share and update apps across a network securely.


**User Interface and Experience**

Corey talks about the design and usability of 'Tilde Friends'. He focuses on making the platform easy and enjoyable to use, with features that update in real time.


**Future Plans and Community**

Corey sees 'Tilde Friends' as a project driven by his personal passion. He's excited about adding new features, like building a community around the platform and allowing apps to work together more effectively. He also mentions potential expansion to mobile devices.

[tildefriends](https://www.tildefriends.net/)


{{< rawhtml >}}
  <div style="height: 20vh" />
{{< /rawhtml >}}
