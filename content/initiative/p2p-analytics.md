---
title: P2P Analytics
display: public
display: draft
description: Analytics + consent
category: initiative
author: [nonlinear]
date: 2023-06-30
---


> The problem of analytics on open source

> how to ethically surveil users?

> how do we ensure anonymity, privacy?

> preserving PII

> where to flag

> passionate users: how to create a UXR pipeline

> And if we're aiming for goldilocks (both too much and too little are bad) shouldn't we have an SSB simulator to test how features warp topology?

> how to gather consent

> analytics specs, governance

> system-only label with metadata
