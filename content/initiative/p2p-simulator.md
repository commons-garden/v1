---
title: P2P Simulator
display: public
display: draft
description: network simulator
category: initiative
author: [nonlinear, cblgh, mixmix]
date: 2023-06-30
---

- [shared notes](https://hackmd.io/@praxis/p2p-simulator)
- [signal group](https://signal.group/#CjQKIM5tco8NGWVYI0BXItiYaxJWBi7A9d8aZ0NiTzSpcsO2EhCPKAqj_0qD7g_rpyFe9Q8I) (announce yourself)
- [SSB simulator](https://mixmix.gitlab.io/butt-sim/)
- [ssbc/netsim: secure scuttlebutt network simulator](https://github.com/ssbc/netsim)
- [Holochain Playground](https://holochainplayground.github.io/)


## Assumptions to validate

> Simulations as a laboratory, providing data scientists with tools prove shit mathematically

> A tool to validate arguments

> It also detects attacks way earlier. By reverse engineering strategies and proposing patches. Because we will be attacked. Of course

> Simulations can detect attacks and propose vaccinations

> How can SSB have a good relationship with data scientists, so researchers can use it for their papers?

> Manuel de landa idea: A layer of simulation tools between hunch and testing, for science.

> A playground

> Not for proving something but creating a path for people to simulate and bring results.

> How to attract data scientists that want to show their chops, but corporate social media keep it lock and key?

> Model communities to test: No problem with surveillance coz it tests there. We can have packs, different topologies.
