---
title: nonlinear
imageType: jpg
description: the anagram of my name is anarchist fool
display: public
category: gardener
date: 2023-03-07
---

### Projects:

- [nonlinear](http://nonlinear.nyc) 
- [praxis](https://praxis.nyc) 
- [commons.garden](https://commons.garden) (this one)

### Find me on:

- [mastodon](https://social.praxis.nyc/@nonlinear) 
- [instapaper](https://www.instapaper.com/p/nonlinear) 
- [instagram](https://instagram.com/nonlinear) 
- [savee](https://savee.it/nonlinear)