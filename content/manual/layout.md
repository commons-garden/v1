---
title: site layout
display: public
description: Describing the site layout
category: manual
date: 2023-06-03
---

Site is hugo and it has 3 main layouts:

- `index` is the cover page
- `list` is any category list
- `single` is any article

Site is hosted on gitlab, [ask permission](/about/help) if you want to help.