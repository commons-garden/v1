---
title: Category
display: public
category: manual
description: How to manage categories
date: 2023-11-28
---

## TODO

- [ ] explain categories 
- [ ] loop thru 1st item on `data/category.json`
	- [ ] if value is `"list"` just add a "View list" link pointing to `tutorial/{key}`

All commons.garden posts fall under a category

- title: description
- group: group category belongs (controlled)
- display: [View list](/manual/display)
- pluralize: certain category titles can be plurarized, some not

(list all categories, with links)