---
title: Not Commons
type: public
status: ongoing
description: Engaging with profiteers
category: initiative
author: [nonlinear, cblgh]
date: 2023-06-05
---

## Useful links

- Initial discussion: [peer-to-peer commercialization](https://cblgh.org/p2p-commercialization/)
- [Common-Pool Resource: Definition, How It Works, and Examples](https://www.investopedia.com/terms/c/common-pool.asp)
- [Against the Grain: A Deep History of the Earliest States](https://en.wikipedia.org/wiki/Against_the_Grain:_A_Deep_History_of_the_Earliest_States?useskin=vector)

## Analogies

1. royals vs nomads
2. definition of boundaries in therapy
3. gentrification

## Intake 

- Definition of done: A healthy immune system
	- Agreed boundaries
	- Sanctions
	- Rewards
- Deliverables
	- takeaways for public awareness
	- promotional pieces
- Who should be on the table?
	- entire SSB community
- What skillsets we need?
	- sociologists
	- anthropologists
- Who does it affect? Which groups are affected and not on the table? 
	- commons other than SSB
	- mastodon? 
	- IPFS
	- Other open source computing languages
- Who benefits?
	- Future SSB users
	- Marginalized communities
	- Good commercial actors
- Post-success opportunities
	- engaged community
	- strenghtened diplomatic channels

## Assumptions to validate

> We wont ever SOLVE capitalists squandering commons. we can't win. we can only mitigate.

> Can commons and for profit live in balance? What is a good neighbor? What is a bad neighbor?

> What is lock in?

> What are the metrics to detect bad faith?

> Platform owners are rent-seekers and they are not aligned with the users, in similar way as that of landlords and renters. making the space cool from a place of scrappiness invites a lot of interesting people and perspectives, which further increases the coolness of being there. this causes corporations to want to exploit that niche in the same way that gentrification happens to scruffy punk/art/diy neighbourhoods

> How we punish get protected from bad nodes?

> Do tide lift lifts all boats? Who gets behind? How to protect them?

> Success case stories of commercial on open source environments?

> What do we want (not what we don't want)? 

> What do we tolerate? What we dont tolerate?

> Failure case stories of commercial on open source environments?

> The gentrification process also drives out the initiators in a similar way in this arena, i feel like. as processes are bureaucratized and financialization enters the frame, some actors either don't want to or can't continue to exist in that space

