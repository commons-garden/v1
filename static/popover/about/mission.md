---
title: Mission statement
type: public
description: Our goals
category: about
date: 2023-03-07
---
The Scuttlebutt ecosystem is comprised of informal alliances, projects and organisations. Our mission is to support a diverse and vibrant ecosystem by:

- Supporting **alignment and coordination** among actors
- Identifying **emergent problems** and tensions
- Facilitating communication and **dispute resolution**
- Hosting (or pointing to) existing **specifications**
- Guiding the **production of new** specifications 
- Producing (or pointing to) **comparative** documentation
- **Mapping** the ecosystem

## 1. Guarding and enhancing the commons

(what projects have to have for us to sponsor it? why? be explicit)

- usability
- security
- aligning parallel work
	- connections
- conflict resolution
- coalescing (standards, patterns)
- code of conduct
- technical standards

## 2. Low-stress emotional labor

Maintenance never ends, so organization ideas win speed

## 3. From nouns to verbs

An effort *towards*, not a state of perfection

## 4. We serve the community

- start with community we know (scuttlebutt)
- no have time to do specs? we can help.
- we can resource that for you!
- you can focus on great experiences instead
- we get it off your plate
- conflict resolution
- reaching agreements
- accelerator