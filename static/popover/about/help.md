---
title: How to help
type: public
description: Ways to help us
category: about
date: 2023-03-07
---

## Per initiative

Check the [Open Initiatives](/about/initiatives) list and [reach out](/about/contact) if you...

1. ...have field knowledge on the subject and 
1. ...are willing to participate on discussions


## Overall organization

If you have experience managing coalition groups, [we'd love to hear from you](/about/contact).

## Keep in touch

Commons Garden is on initial stages, keep informed via our [Open Collective](opencollective.com/commons-garden) page.

## Contact us

1. Via [praxis.nyc@protonmail.com](mailto:praxis.nyc@protonmail.com?subject:commons.garden) email
1. Via [signal group](https://signal.group/#CjQKIAneWf9J6yoVIAKUjLdLynBTbQxuaTEVHbpaLioQhdL-EhAybesuQH-M_Uy8AvcJkEeW)
