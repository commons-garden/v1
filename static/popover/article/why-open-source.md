---
title: Why Open Source?
type: hidden
category: article
date: 2023-03-07
---

- [ ] why is open source worth the fight?
- [ ] why donate your resources?
- [ ] what are other ways to help?
- [ ] commons as umbrella term